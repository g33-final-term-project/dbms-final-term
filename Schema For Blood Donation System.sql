USE BloodDonation;

-- Donor details Table
CREATE TABLE donor_details (
    donor_id INT IDENTITY(1,1) PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    contact_number VARCHAR(15) NOT NULL,
    age INT NOT NULL,
    gender VARCHAR(10) NOT NULL,
    blood_type VARCHAR(5) NOT NULL,
    address VARCHAR(255) NOT NULL
);

-- Admin info Table
CREATE TABLE admin_info (
    admin_id INT IDENTITY(1,1) PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    username VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL
);

-- Blood groups Table
CREATE TABLE blood (
    blood_id INT IDENTITY(1,1) PRIMARY KEY,
    blood_group VARCHAR(5) NOT NULL
);


-- Pages Table
CREATE TABLE pages (
    page_id INT IDENTITY(1,1) PRIMARY KEY,
    page_name VARCHAR(255) NOT NULL,
    page_type VARCHAR(50) NOT NULL,
    page_data TEXT NOT NULL
);

-- Contact info Table
CREATE TABLE contact_info (
    contact_id INT IDENTITY(1,1) PRIMARY KEY,
    address VARCHAR(255) NOT NULL,
    email VARCHAR(100) NOT NULL,
    phone_number VARCHAR(15) NOT NULL
);

-- Contact queries Table
CREATE TABLE contact_query (
    query_id INT IDENTITY(1,1) PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    email VARCHAR(100) NOT NULL,
    phone_number VARCHAR(15) NOT NULL,
    message TEXT NOT NULL,
    submission_date DATETIME DEFAULT CURRENT_TIMESTAMP,
    status INT DEFAULT NULL
);

-- Query statuses Table
CREATE TABLE query_stat (
    status_id INT PRIMARY KEY,
    status_name VARCHAR(50) NOT NULL
);